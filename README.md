
### 使用说明 
>安装前，必须先安装docker和docker-compose 
>启动: docker-compose up -d 
>停止: docker-compose down 

1. portalui: 7000映射80，随便改docker-compose中定义 
2. portalapi: udp:1812 1813 50100 
3. portalmysql: 不暴露端口3306 
4. portalprometheus: 开放7001，这个只是测试。也不需要暴露 
5. portalopenldap：不暴露端口389不用暴露出来 
6. portaladminer: 是为了方便操作mysql  7003,后期关闭即可 
7. portalphpldapadmin 是为了方便操作ldap  7002,后期关闭即可 

> 6和7只是调试工具 
>用户名:admin  
>密码：Xxxxx.ai@123 


####portal.yaml配置文件中修改ac地址 
````
ac:
  brand: 2011 
  timeout: 5 
  nas_port: 2000 
  local_port: 50100 
  nas_ip: "18.18.2.3" 
  coa_port: 3799 # ac的3799端口，开启portal后，硬件设备会开启这个udp服务端口 
  secret: portal 
  method: chap # 是pap认证？还是chap挑战,二种任取其一,推荐chap。安全一点 
  ver: v2 #v1或者是v2,v2更安全 
  fileds: ["userip"] 
  global: "global" 
```
1. 修改ac的地址  nas_ip: "18.18.2.3",这里修改为你的ac地址,其它勿动
2. brand: 2011是华为的code
